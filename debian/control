Source: libpam-mklocaluser
Section: admin
Priority: optional
Maintainer: Debian Edu Developers <debian-edu@lists.debian.org>
Uploaders: Petter Reinholdtsen <pere@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://www.skolelinux.org/
Vcs-Browser: https://salsa.debian.org/debian-edu/upstream/libpam-mklocaluser
Vcs-Git: https://salsa.debian.org/debian-edu/upstream/libpam-mklocaluser.git

Package: libpam-mklocaluser
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         libpam-python
Suggests: libpam-ccreds | libpam-sss,
Description: Configure PAM to create a local user if it do not exist already
 When the user logs in for the first time, a local POSIX user account is
 created in /etc/passwd and primary group created in /etc/group, and a
 local home directory is created in /home.
 .
 This is useful on roaming computers when the password is set up to be
 cached by for example libpam-ccreds or sssd to allow login without
 network connectivity using the password provided by a network
 authentication service like Kerberos or LDAP.
 .
 Other than the caching mechanisms of libpam-ccreds or libpam-sssd /
 libnss-sssd, the libpam-mklocaluser approach makes the LDAP accounts
 more persistent on the roaming workstation and robust again credentials
 cache deletions and such.
 .
 The pam_mklocaluser PAM module additionally allows for execution of
 additional hook scripts when users log in to such a roaming computer for
 the first time.
